[![build status](https://gitlab.com/vinicius_raupp/laravel/badges/master/build.svg)](https://gitlab.com/vinicius_raupp/laravel/commits/master)

### Ambiente de devovimento local ###

```shell
git clone https://gitlab.com/vinicius_raupp/laravel.git && cd laravel && cp .env.example .env
```
```shell
docker run -v $PWD:/app composer composer install
```
```shell
docker-compose -f docker-compose-dev.yml up -d
```
```shell
docker container exec laravel_web_1 chmod 777 -R storage bootstrap/cache
```
```shell
docker container exec laravel_web_1 php artisan key:generate
```